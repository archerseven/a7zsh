# ArcherSeven's zsh configuration main file.

HISTFILE=~/.histfile.zsh
HISTSIZE=10000
SAVEHIST=100000
bindkey -e

alias ls='ls --color=auto --group-directories-first'
alias mount='mount -l'
alias sshfs='sshfs -o follow_symlinks'
alias emacs='emacs -nw'
alias nvm="unset HISTFILE; exit"

if which dircolors > /dev/null; then
	eval "`dircolors -b`"
	eval "`dircolors -b ~/.zsh/dircolors`"
fi

# The following lines were added by compinstall
zstyle :compinstall filename '/home/max/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
source ~/.zsh/zshkeys

#Find a text-mode editor
if type vim > /dev/null
then
	TE="vim"
elif type vi > /dev/null
then
	TE="vi"
elif type nano > /dev/null
then
	TE="nano"
else
	TE=''
fi

#For graphics environments, try code first.
if type code > /dev/null
then
	GE="code -w --user-data-dir /tmp/code/"
else
	GE=${TE}
fi



if [ "$TERM" != "linux" -a "$DISPLAY" != "" ]
then
	export EDITOR=${GE}
	export BROWSER="firefox"
else
	export EDITOR=vim
	export BROWSER=links
fi

case "$(uname -s)" in
    Linux*)     OS_CHAR="";;
    Darwin*)    OS_CHAR='⌘ '
                alias ls='gls --color=auto --group-directories-first'
                ;;
    CYGWIN*)    OS_CHAR='⊞';;
    MINGW*)     OS_CHAR='⊞';;
    *)          OS_CHAR='?'
esac

source $HOME/.zsh/zshps1

setopt no_autoremoveslash
setopt interactive_comments
setopt no_beep
